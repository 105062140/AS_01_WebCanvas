# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed



最上方的三個輸入空格，分別代表著想要顯示的文字，顯示文字位置的X軸與Y軸，輸入完成按下Enter即可顯示，實作於printtext()

而下方的download按鈕則可下載當前畫布的內容，儲存格式是png檔，實作於download()

接著則是color的選項，這部分可控制所有顯示的文字與圖像顏色，預設值為黑色，實作於changecolor(var)

下方的typeface則可選擇顯示文字的字體(僅對英文有效)，預設值為Arial字體，實作於typeface(var)

size負責控制文字的字體大小以及畫筆的粗細，還有畫出來的圖形邊框大小，實作於changesize(var)

最下方的控制欄位分別實現幾項不同的功能，第一個是fill，如果打勾，在畫出圖形時內部會自動上色，色彩則是與邊框相同，實作於draw()裡面的判斷式

第二個部份則是選取繪畫工具，實作於changeshape(var)，這個函式會改變名為shape的變數，而滑鼠點擊，滑鼠移動，與滑鼠放開分別會觸發Start()，Drawing()與Draw()，
這幾個部份的函式皆會判斷當前的shape，而做出不一樣的動作，根據選取工具，滑鼠也會有所改變，筆的話是十字游標，圖形則是斜箭頭，橡皮擦則是十字箭頭，同樣也實作於changeshape(var)

最後則是三個功能，undo，redo與reset，undo可以回上一步，redo則是到下一步，reset則是清空畫布，分別由Undo()，Redo()，reset()實作
